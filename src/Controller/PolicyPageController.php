<?php
/**
 * @file
 * Contains \Drupal\biopama_policy\Controller\PolicyPageController.
 */

namespace Drupal\biopama_policy\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_policy module.
 */
class PolicyPageController extends ControllerBase {
  public function content() {
    $element = array(
      '#theme' => 'policy_page',
    );
    return $element;
  }
}